# Formation Git

## Commandes liées aux branches

- git branch
- git switch / git checkout
- git merge (--no-ff, --ff, --ff-only, --squash)
- git rebase
- git cherry-pick

